<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@home');
Route::get('/services', 'HomeController@services');
Route::get('/business_applications', 'HomeController@business_applications');
Route::get('/cloud_computing', 'HomeController@cloud_computing');
Route::get('/mobile_applications', 'HomeController@mobile_applications');
Route::get('/web_application', 'HomeController@web_application');
Route::get('/aboutus', 'HomeController@aboutus');
Route::get('/contact', 'HomeController@contact');



