<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
	public function index()
    {
        return view('welcome');
    }
    public function services()
    {
        return view('services');
    }
    public function business_applications()
    {
        return view('business_applications');
    }
    public function cloud_computing()
    {
        return view('cloud_computing');
	}public function mobile_applications()
    {
        return view('mobile_applications');
	}public function web_application()
    {
        return view('web_application');
	}public function home()
    {
        return view('home');
	}
	
	public function aboutus()
    {
        return view('aboutus');
    }
    public function contact()
    {
        return view('contact');
	}
}
