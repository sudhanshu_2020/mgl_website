@extends('layouts.app')

@section('content')
<style>


</style>


<div id="myCarousel" class="carousel slide carousel-fade backgroundCustom" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>Strong and Expereinced <br>ASP.NET Team.</h4>
                            <p>Best ASP.NET Development Company India.
                                Hire Best ASP.NET Developers.</p>
                            <a href="{{'web_application'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\dot-net-services.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>PHP Development<br> Company India</h4>
                            <p>
                                Build Custom & Dynamic web Apps with our PHP Developers.
                                Hire PHP developers India. <br>
                            </p>
                            <a href="{{'web_application'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\phpServicesSl.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>Android App Development <br>Company India.</h4>
                            <p>
                                Build Android apps as per your need with our Android App Developers.
                                <br>
                                Hire Android app developers India. 
                            </p>
                            <a href="{{'mobile_applications'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\MobileApps.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
</div>
<!--slide end-->

@endsection
