@extends('layouts.app')

@section('content')
<style>
	.map-responsive {
		overflow: hidden;
		padding-bottom: 50%;
		position: relative;
		height: 0;
	}

	.map-responsive iframe {
		left: 0;
		top: 0;
		height: 100%;
		width: 100%;
		position: absolute;
	}
</style>
<!-- <div class="page-heading wow fadeIn" data-wow-duration="0.5s">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading-content-bg wow fadeIn" data-wow-delay="0.75s" data-wow-duration="1s">
					<div class="row">
						<div class="heading-content col-md-12">
							<p><a href="{{ url('home') }}">Home</a> / <em> Contact Us</em></p>
							<h2>Contact <em>Us</em></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->



<div id="myCarousel" class="carousel slide carousel-fade backgroundCustom" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<div class="mask flex-center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-7 col-12 order-md-1 text-center order-2">
							<h4>Present your <br>
								awesome product</h4>
							<p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
								necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
							<a href="#">BUY NOW</a>
						</div>
						<div class="col-md-5 col-12 order-md-2 order-1"><img src="https://i.imgur.com/NKvkfTT.png" class="mx-auto" alt="slide"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="carousel-item">
			<div class="mask flex-center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-7 col-12 order-md-1 text-center order-2">
							<h4>Present your <br>
								awesome product</h4>
							<p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
								necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
							<a href="#">BUY NOW</a>
						</div>
						<div class="col-md-5 col-12 order-md-2 order-1"><img src="https://i.imgur.com/duWgXRs.png" class="mx-auto" alt="slide"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="carousel-item">
			<div class="mask flex-center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-7 col-12 order-md-1 text-center order-2">
							<h4>Present your <br>
								awesome product</h4>
							<p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
								necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
							<a href="#">BUY NOW</a>
						</div>
						<div class="col-md-5 col-12 order-md-2 order-1"><img src="https://i.imgur.com/DGkR4OQ.png" class="mx-auto" alt="slide"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
</div>
<!--slide end-->





<!-- <div id="map"></div> -->


<section>
	<div class="contact-content wow fadeIn" data-wow-delay="0.5s" data-wow-duration="1s">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="send-message">
						<div class="sep-section-heading">
							<h2>Send Us <em>Message</em></h2>
						</div>
						<form id="contact_form" action="#" method="POST" enctype="multipart/form-data">
							<div class="row">
								<div class=" col-md-4 col-sm-4 col-xs-6">
									<input type="text" class="blog-search-field" name="s" placeholder="Your name..." value="">
								</div>
								<div class="col-md-4 col-sm-4 col-xs-6">
									<input type="text" class="blog-search-field" name="s" placeholder="Your email..." value="">
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input type="text" class="subject" name="s" placeholder="Subject..." value="">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<textarea id="message" class="input" name="message" placeholder="Message..."></textarea>
								</div>
							</div>
							<div class="row">
								<div class="submit-coment col-md-12">
									<div class="primary-button">
										<a href="#">Send now <i class="fa fa-paper-plane"></i></a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-4">
					<div class="contact-info">
						<div class="sep-section-heading">
							<h2>Contact <em>Informations</em></h2>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati accusamus a iure nulla, sed non ex nobis eius esse distinctio imps sunt quia sint quis quisquam odio repellat.</p>
						<div class="info-list">
							<ul>
								<li><i class="fa fa-phone"></i><span>+1 123 489-5748</span></li>
								<li><i class="fa fa-envelope"></i><span>mglitservices@gmail.com</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<div class="contact-us wow fadeIn mt-4" data-wow-delay="0.5s" data-wow-duration="1s">

	<div class="container-fluid">
		<div class="map-responsive">
			<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=Eiffel+Tower+Paris+France" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
		</div>
	</div>
</div>



@endsection