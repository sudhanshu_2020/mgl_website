@extends('layouts.app')

@section('content')

<!-- 
<div class="page-heading wow fadeIn" data-wow-duration="0.5s">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading-content-bg wow fadeIn" data-wow-delay="0.75s" data-wow-duration="1s">
					<div class="row">
						<div class="heading-content col-md-12">
							<p><a href="index.html">Homepage</a> / <em> About Us</em></p>
							<h2>About <em>Us</em></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->




<div id="myCarousel" class="carousel slide carousel-fade backgroundCustom" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<div class="mask flex-center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-7 col-12 order-md-1 text-center order-2">
							<h4>Present your <br>
								awesome product</h4>
							<p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
								necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
							<a href="#">BUY NOW</a>
						</div>
						<div class="col-md-5 col-12 order-md-2 order-1"><img src="https://i.imgur.com/NKvkfTT.png" class="mx-auto" alt="slide"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="carousel-item">
			<div class="mask flex-center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-7 col-12 order-md-1 text-center order-2">
							<h4>Present your <br>
								awesome product</h4>
							<p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
								necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
							<a href="#">BUY NOW</a>
						</div>
						<div class="col-md-5 col-12 order-md-2 order-1"><img src="https://i.imgur.com/duWgXRs.png" class="mx-auto" alt="slide"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="carousel-item">
			<div class="mask flex-center">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-7 col-12 order-md-1 text-center order-2">
							<h4>Present your <br>
								awesome product</h4>
							<p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
								necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
							<a href="#">BUY NOW</a>
						</div>
						<div class="col-md-5 col-12 order-md-2 order-1"><img src="https://i.imgur.com/DGkR4OQ.png" class="mx-auto" alt="slide"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
</div>
<!--slide end-->








<section class="amazing-dashboard-block  no-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7  col-sm-12 wow wdth_100 slideInLeft" style="visibility: visible;">
				<div class="col-md-12">
					<div class="">
						<br>
						<br>
						<div class="text ">
							<h6>About MGL</h6>
							<h3>We are a Software Development Company &amp; We Love Solving Business Problems</h3>
							<p>
								MGL offers multiple channels for outsourced software development based on unique requirements of our clients/partners.
								MGL is software development and web development Company based in India. we are providing IT solutions and IT-enabled services. Ever since our establishment, we are growing every day to meet the growing demands of our clients and surpass their expectations.
								<br>
								<br>Our expertise in providing industry-specific services and custom solutions has enabled us to deliver absolute business solutions to our worldwide clients. We are providing our services to small as well as big corporate houses including individual entrepreneurs using our unmatched expertise and cutting edge technology.</p>
							<div class="clearfix"></div>
							<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 wdth_100 no-padding wow slideInRight" style="visibility: visible;">
				<img style="width: 100%;" class="img-responsive mobile_none" src="resources/assets/images/about.jpg" alt="">
			</div>
		</div>
	</div>
</section>





<div class="services-first about-services wow fadeIn" data-wow-delay="0.5s" data-wow-duration="1s">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="service-item">
					<i class="fab fa-php"></i>
					<div class="text-content">
						<h6>PHP Development</h6>
						<p>
							Dynamic and robust PHP solutions that not only let you establish your brand but
							also achieve the desired success in the competitive market.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="service-item">
					<i class="fab fa-angular"></i>
					<div class="text-content">
						<h6>Angular Development</h6>
						<p>
							Explore the Customized, Effective and Quality-driven Angular Development Services with the team that know it from the core.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="service-item">
					<i class="fab fa-node"></i>
					<div class="text-content">
						<h6>Node.js Development</h6>
						<p>
							We maintain transparency with our clients. Our values and business ethics has given us repeated customers.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="service-item">
					<i class="fab fa-react"></i>
					<div class="text-content">
						<h6>React Native Development</h6>
						<p>
							Build stunning and highly functional cross-platform applications using React Native development services and bring your business on Android & iOS platforms concurrently.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="service-item">
					<i class="fab fa-android"></i>
					<div class="text-content">
						<h6>Android Application</h6>
						<p>
							Mobile ApplicationsWith the rapid growth of the smart phone and mobile internet market, the mobile devices have become the primary interface to internet and social networks.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="service-item">
					<i class="fab fa-windows"></i>
					<div class="text-content">
						<h6>WCF Development</h6>
						<p>
							SummationIT provides expert WCF development services. We have delivered multiple projects on WCF and .net development since our inception.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section>
	<div class="more-about-us">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<img src="resources\assets\images\our-Approatch.jpg" alt="">
				</div>
				<div class="col-md-6">
					<div class="right-content">
						<span>MGL.</span>
						<h4>Our Approach</h4>
						<p>
							At MGL, we consider each of our clients as our strategic partners. Together with them we work on finding
							optimal solutions to their problems.<br><br> Each of our solutions is tailor-made for your business needs and backed by
							constant innovation. At the same time, our solutions are completely scalable and designed to grow with your
							business.

							Our goal is to bring out new things in the market with each line of code that contributes something in the future and fits into your budget.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="our-clients">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-heading">
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<div class="text-content">
							<h2>Our Happy Clients</h2>
							<span>Here are our happy clients</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="owl-clients" class="owl-carousel owl-theme">
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="http://placehold.it/180x120" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
						<div class="item">
							<img src="resources\assets\images\services\php1.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>





<script>
	$('#myCarousel').carousel({
		interval: 3000,
	})
</script>
@endsection