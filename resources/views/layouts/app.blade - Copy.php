<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'APP NAME') }}</title>

    <!-- Scripts -->
	<script src="{{ asset('public/js/app.js') }}"></script>
	<script src="{{ asset('public/js/jquery-3.1.1.min.js') }}"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('public/js/jquery.validate.min.js') }}"></script>
	
	<link rel="stylesheet" href="{{ asset('public/DataTables-1.10.18/css/jquery.dataTables.css') }}" />
	<script type="text/javascript" charset="utf8" src="{{ asset('public/DataTables-1.10.18/js/jquery.dataTables.js') }}"></script>
	
	
	<link rel="stylesheet" href="{{ asset('public/js/jquery-data-table/dataTables.bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/js/jquery-data-table/buttons.bootstrap.min.css') }}" />
	
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('public/open-iconic/font/css/open-iconic.css') }}" rel="stylesheet">
	<style>
		span.required{
			color:red;
		}
		label.error{
			color:red;
		}
	</style>
</head>
<body>
    <div id="app">
        <!-- Start Header -->

		<!-- End Header -->
        <main class="py-4">
            @yield('content')
        </main>

		<!-- Start Footer -->
		
		<!-- End Footer -->

    </div>
	
	<script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
</body>
</html>
