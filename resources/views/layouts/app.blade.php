<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="keywords" content="Software,MobileApplicaton,WebApplication,JavaScript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="MGL">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>MGL</title>

	<!-- Scripts -->
	<link rel="stylesheet" href="{{ asset('resources/assets/css/bootstrap.css') }}" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
	<!-- 
	<link rel="stylesheet" href="{{ asset('resources/assets/css/font-awesome.min.css') }}" /> -->
	<link rel="stylesheet" href="{{ asset('resources/assets/css/main.css') }}" />
	<!-- Slider Pro Css -->
	<link rel="stylesheet" href="{{ asset('resources/assets/css/sliderPro.css') }}" />
	<!-- Owl Carousel Css -->
	<link rel="stylesheet" href="{{ asset('resources/assets/css/owl-carousel.css') }}" />
	<!-- Flat Icons Css -->
	<link rel="stylesheet" href="{{ asset('resources/assets/css/flaticon.css') }}" />
	<!-- Animated Css -->
	<link rel="stylesheet" href="{{ asset('resources/assets/css/animated.css') }}" />

	<link rel="stylesheet" href="{{ asset('resources\assets\css\customAll.css') }}" />


</head>

<body>
	<div id="app">
		<!-- Start Header -->

		<!-- <div class="preloader">
			<div class="preloader-bounce">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div> -->

		<div id="search">
			<button type="button" class="close">×</button>
			<form>
				<input type="search" value="" placeholder="type keyword(s) here" />
				<button type="submit" class="primary-button">Search <i class="fa fa-search"></i></button>
			</form>
		</div>


		<header class="site-header wow fadeIn" data-wow-duration="1s">
			<div id="main-header" class="main-header">
				<div class="container clearfix">
					<div class="logo">
						<a href="{{ url('home') }}"></a>
					</div>
					<div id='cssmenu'>
						<ul>
							<li><a href="{{ url('home') }}">Home</a></li>
							<li class='active'><a href="{{ url('services') }}">Services</a>
								<ul>
									<li><a href="{{ url('mobile_applications') }}">Mobile Apps</a></li>
									<li><a href="{{ url('web_application') }}">Web Application</a></li>
									<li><a href="{{ url('business_applications') }}">Business Applications</a></li>
									<li><a href="{{ url('cloud_computing') }}">Cloud Computing</a></li>

								</ul>
							</li>
							<!-- <li class='active'><a href='#'>Blog</a>
								<ul>
									<li><a href='#'>Sidebar</a>
										<ul>
											<li><a href='blog_listing_sidebar.html'>Blog Classic</a></li>
											<li><a href='blog_grid_sidebar.html'>Blog Grid</a></li>
										</ul>
									</li>
									<li><a href='#'>No Sidebar</a>
										<ul>
											<li><a href='blog_listing_no_sidebar.html'>Blog Classic</a></li>
											<li><a href='blog_grid_no_sidebar.html'>Blog Grid</a></li>
										</ul>
									</li>
									<li><a href="single_post.html">Single Post</a></li>
								</ul>
							</li> -->
							<li><a href="{{ url('aboutus') }}">About Us</a></li>
							<li><a href="{{ url('contact') }}">Contact Us</a></li>
							<li>
								<a href="#search"><i class="fa fa-search"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</header>

		<!-- End Header -->
		<main>
			@yield('content')
		</main>

		<!-- Start Footer -->

		<section>
			<div class="recent-car similar-car wow fadeIn" data-wow-duration="1s">
				<div class="container">
					<div class="recent-services-content">
						<div class="row">
							<div class="col-md-12">
								<div class="section-heading">
									<!-- <div class="icon">
								<i class="fa fa-car"></i>
							</div> -->
									<div class="text-content">
										<h2>Technologies We Work For</h2>
										<span>We specialise in web design & development, customised stand alone software development as well as Mobile Applications.</span>
									</div>
								</div>
							</div>
						</div>
						<div id="owl-similar" class="owl-carousel owl-theme">
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="#">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources\assets\images\services\php1.jpg" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>PHP Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">For Sale</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/Angular.jpg" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>Angular Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/nodejs.jpg" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>Node.js Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/reactJs.jpg" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>React Native Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/Android.png" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>Android Application</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/wcf.png" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>WCF Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/SEO.png" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>SEO</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/ui-ux.png" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>UI UX Design</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">For Sale</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/g.jpg" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>GRAPHIC DESIGN</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/Ms-Sql.jpg" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>Microsoft SQL Server Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
							<div class="item services-item-1">
								<div class="thumb-content">
									<!-- <div class="services-banner-1">
								<a href="{{'services'}}">Web Design</a>
							</div> -->
									<a href="{{'services'}}"><img src="resources/assets/images/services/dotnetcore.png" alt=""></a>
								</div>
								<div class="down-content">
									<a href="{{'services'}}">
										<h4>ASP.Net Core Development</h4>
									</a>
									<span>& Services</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>



		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="footer-item">
							<div class="about-us">
								<h2>About Us</h2>
								<p>
									MGL (Pvt.) Ltd., is a globally focused IT solutions and services provider based in India.
									We offer end-to-end information technology solutions and services to businesses and organizations around the world.
								</p>
								<ul>
									<li><a href="#"><i class="fab fa-facebook"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-behance"></i></a></li>
									<li><a href="#"><i class="fab fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="footer-item">
							<div class="what-offer">
								<h2>Services</h2>
								<ul>
									<li><a href="{{ url('mobile_applications') }}">Mobile Apps</a></li>
									<li><a href="{{ url('web_application') }}">Web Application</a></li>
									<li><a href="{{ url('business_applications') }}">Business Applications</a></li>
									<li><a href="{{ url('cloud_computing') }}">Cloud Computing</a></li>
									<!-- <li><a href="{{ url('services') }}">Website Maintenance & Support</a></li>
									<li><a href="{{ url('services') }}">Software Development</a></li> -->
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="footer-item">
							<div class="need-help">
								<h2>Link</h2>
								<ul>
									<li><a href="{{ url('home') }}">Home</a></li>
									<li><a href="{{ url('aboutus') }}">About Us</a></li>
									<li><a href="{{ url('contact') }}">Contact Us</a></li>

								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="footer-item">
							<div class="our-gallery">
								<h2>Our Gallery</h2>
								<ul>
									<li><a href="{{ url('services') }}"><img src="resources\assets\images\services\angular-sm.png" alt=""></a></li>
									<li><a href="{{ url('services') }}"><img src="resources\assets\images\services\dotnet-sm.png" alt=""></a></li>
									<li><a href="{{ url('services') }}"><img src="resources\assets\images\services\API11.png" alt=""></a></li>
									<li><a href="{{ url('services') }}"><img src="resources\assets\images\services\dotnet-sm.png" alt=""></a></li>
									<li><a href="{{ url('services') }}"><img src="resources\assets\images\services\m.png" alt=""></a></li>
									<li><a href="{{ url('services') }}"><img src="resources\assets\images\services\api-sm11.png" alt=""></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="footer-item">
							<div class="quick-search">
								<h2>Quick Search</h2>
								<input type="text" class="footer-search" name="s" placeholder="Search..." value="">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="sub-footer">
							<p>Copyright 2020. All rights reserved by: <a href="#">MGL</a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<!-- End Footer -->

	</div>

	<script src="{{ asset('resources/assets/js/jquery-1.11.0.min.js') }}"></script>

	<!-- Slider Pro Js -->
	<script src="{{ asset('resources/assets/js/sliderpro.min.js') }}"></script>
	<!-- Slick Slider Js -->
	<script src="{{ asset('resources/assets/js/slick.js') }}"></script>
	<!-- Owl Carousel Js -->
	<script src="{{ asset('resources/assets/js/owl.carousel.min.js') }}"></script>
	<!-- Boostrap Js -->
	<script src="{{ asset('resources/assets/js/bootstrap.min.js') }}"></script>
	<!-- Boostrap Js -->
	<script src="{{ asset('resources/assets/js/wow.animation.js') }}"></script>
	<!-- Custom Js -->
	<script src="{{ asset('resources/assets/js/custom.js') }}"></script>
</body>

</html>