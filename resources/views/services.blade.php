@extends('layouts.app')

@section('content')

<style>


</style>




<div id="myCarousel" class="carousel slide carousel-fade backgroundCustom" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>Strong and Expereinced <br>ASP.NET Team.</h4>
                            <p>Best ASP.NET Development Company India.
                                Hire Best ASP.NET Developers.</p>
                            <a href="{{'web_application'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\dot-net-services.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>PHP Development<br> Company India</h4>
                            <p>
                                Build Custom & Dynamic web Apps with our PHP Developers.
                                <br>Hire PHP developers India. <br>
                            </p>
                            <a href="{{'web_application'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\phpServicesSl.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>Android App Development <br>Company India.</h4>
                            <p>
                                Build Android apps as per your need with our Android App Developers.
                                <br>
                                Hire Android app developers India.
                            </p>
                            <a href="{{'mobile_applications'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\MobileApps.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
</div>
<!--slide end-->





<section class="design-services mt-4">
    <div class="container">
        <h2 class="design-service-title">
            OUR UI AND UX DESIGN SERVICES AND CAPABILITIES
        </h2>
        <ul class="design-items">
            <li class="design-item">
                <div class="design-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="72" height="71" viewBox="0 0 72 71">
                        <g fill="none" fill-rule="evenodd" stroke="#1476F2">
                            <path d="M44.388 55.84c0 1.1-1.828 2.645-3.013 3.012-.126.487.186 1.397.598 2.709 1.134 3.61-1.371 6.467-4.672 6.467-2.366 0-15.465-3.905-17.433-6.601l-5.172-7.85M24.072 63.93L22.441 71M45.403 36c1.95 2.64 4.665 6.253 4.665 7.98 0 2.483-3.01 2.678-5.134 2.992.464 2.486 2.412 3.68 1.996 4.405-.415.728-6.893 1.729-10.731 1.729M14.696 1c6.138 0 11.98 1.712 15.873 3.316C41.524 8.829 45.275 19.94 45.16 25.92"></path>
                            <path d="M8.609 32.957h21.304C31.625 36 36 39.043 40.565 39.043c4.565 0 7.609-6.974 7.609-9.13 0-.825.053-1.522 0-3.043H10.13C3.537 26.87 1 30.42 1 36v3.043M51.217 34.478L71 48.174V8.609L49.696 23.826"></path>
                        </g>
                    </svg>
                </div>
                <h3 class="design-title">AR experience design</h3>
            </li>
            <li class="design-item">
                <div class="design-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="71" height="58" viewBox="0 0 71 58">
                        <g fill="none" fill-rule="evenodd" stroke="#1476F2" transform="translate(1 1)">
                            <path d="M30.435 33.297v19.676A3.034 3.034 0 0 1 27.39 56H3.043A3.034 3.034 0 0 1 0 52.973V3.027A3.034 3.034 0 0 1 3.043 0h24.348a3.034 3.034 0 0 1 3.044 3.027v21.19"></path>
                            <path stroke-linejoin="round" d="M0 47h30.435M30.435 8H0"></path>
                            <path d="M39 33.297v19.676A3.034 3.034 0 0 0 42.043 56h24.348a3.034 3.034 0 0 0 3.044-3.027V3.027A3.034 3.034 0 0 0 66.39 0H42.043A3.034 3.034 0 0 0 39 3.027v21.19"></path>
                            <path stroke-linejoin="round" d="M69.435 47H39M39 8h30.435"></path>
                            <path d="M57.217 29H42M53 34.08l4.565-4.54L53 25M36 29h3.043M12 29h15.217M16.565 33.08L12 28.54 16.565 24"></path>
                            <circle cx="54.5" cy="51.5" r="1.5"></circle>
                            <circle cx="14.5" cy="51.5" r="1.5"></circle>
                            <path d="M33.043 29H30"></path>
                        </g>
                    </svg>
                </div>
                <h3 class="design-title">Mobile app UX and UI design services</h3>
            </li>
            <li class="design-item">
                <div class="design-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="56" height="72" viewBox="0 0 56 72">
                        <g fill="none" fill-rule="evenodd" stroke="#1476F2" transform="translate(1 1)">
                            <path d="M19.352 56.812H3.1c-1.713 0-3.1-1.375-3.1-3.071V3.07C0 1.374 1.387 0 3.1 0h24.799c1.712 0 3.1 1.374 3.1 3.07v5.553"></path>
                            <path d="M54.424 66.93c0 1.696-1.387 3.07-3.1 3.07H26.526c-1.713 0-3.1-1.374-3.1-3.07V16.26c0-1.697 1.387-3.072 3.1-3.072h24.798c1.713 0 3.1 1.375 3.1 3.071v50.67z"></path>
                            <path stroke-linejoin="round" d="M0 49.681h19.352M23.426 60.87h30.998M30.998 4.116H0"></path>
                            <path d="M4.574 9.688h10.204v9.145H4.574zM4.574 23.891h10.204v9.145H4.574z"></path>
                            <path stroke-linejoin="round" d="M54.424 21.304H23.426"></path>
                            <path d="M28 25.862h21.407v22.304H28zM28 52.181h21.407v4.072H28z"></path>
                            <ellipse cx="39.213" cy="65.435" rx="1.528" ry="1.522"></ellipse>
                            <path stroke-linejoin="round" d="M13.24 52.754h3.1"></path>
                        </g>
                    </svg>
                </div>
                <h3 class="design-title">Cross-platform experiences design</h3>
            </li>
            <li class="design-item">
                <div class="design-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="70" height="75" viewBox="0 0 70 75">
                        <g fill="none" fill-rule="evenodd" stroke="#1476F2">
                            <path d="M35 0v14.17V0zM17.5 4.64l7.16 12.273L17.5 4.64zM4.689 17.32l12.4 7.085-12.4-7.085zM0 34.639h14.318H0zm4.689 17.319l12.4-7.085-12.4 7.085zm60.622 0l-12.4-7.085 12.4 7.085zM70 34.638H55.682 70zm-4.689-17.32l-12.4 7.086 12.4-7.085zM52.5 4.64l-7.16 12.273L52.5 4.64zM25.454 59.83h19.092m-19.092 6.297h19.092M25.454 34.64c0-5.219 4.274-9.447 9.546-9.447M30.228 39.361L35 44.085l4.772-4.724M35 44.085v11.022"></path>
                            <path d="M41.363 72.425h-1.59L38.181 74h-6.364l-1.592-1.575h-1.59m14.318-17.318V53.53c0-2.842 1-5.464 2.995-7.48 3.006-3.034 4.96-6.787 4.96-11.412 0-8.697-7.123-15.746-15.909-15.746-8.787 0-15.908 7.05-15.908 15.746 0 4.625 1.955 8.378 4.96 11.412 1.995 2.015 2.994 4.638 2.994 7.481v1.575"></path>
                        </g>
                    </svg>
                </div>
                <h3 class="design-title">UI and UX consulting</h3>
            </li>
            <li class="design-item">
                <div class="design-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="70" height="65" viewBox="0 0 70 65">
                        <g fill="none" fill-rule="evenodd" stroke="#1476F2">
                            <path d="M62.391 49.372h6.087V1H1.522v48.372h6.087M0 64.488h70"></path>
                            <path d="M54.783 44.837v-4.535c0-3.339-2.726-6.046-6.087-6.046h-3.044c-3.361 0-6.087 2.707-6.087 6.046v4.535M27.391 55.419V54.06c0-4.26-3.36-6.2-7.608-6.2-4.25 0-7.61 1.94-7.61 6.2v1.358M42.609 55.419V54.06c0-4.26-3.36-6.2-7.609-6.2-4.249 0-7.609 1.94-7.609 6.2v1.358M57.826 55.419V54.06c0-4.26-3.36-6.2-7.609-6.2-4.248 0-7.608 1.94-7.608 6.2v1.358M19.783 64.488V61.62c0-4.26-3.36-6.2-7.61-6.2-4.248 0-7.608 1.94-7.608 6.2v2.87"></path>
                            <path d="M35 64.488V61.62c0-4.26-3.36-6.2-7.609-6.2-4.248 0-7.608 1.94-7.608 6.2v2.87M50.217 64.488V61.62c0-4.26-3.36-6.2-7.608-6.2-4.249 0-7.609 1.94-7.609 6.2v2.87M65.435 64.488V61.62c0-4.26-3.36-6.2-7.609-6.2-4.249 0-7.609 1.94-7.609 6.2v2.87M47.174 29.72a4.532 4.532 0 0 0 4.565-4.534v-1.512a4.532 4.532 0 0 0-4.565-4.534 4.532 4.532 0 0 0-4.565 4.534v1.512a4.532 4.532 0 0 0 4.565 4.535z"></path>
                        </g>
                    </svg>
                </div>
                <h3 class="design-title">Design workshops</h3>
            </li>
            <li class="design-item">
                <div class="design-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="72" height="64" viewBox="0 0 72 64">
                        <g fill="none" fill-rule="evenodd" stroke="#1476F2" stroke-linejoin="round">
                            <path d="M67.957 63h-51.74a3.033 3.033 0 0 1-3.043-3.024v-4.537H36l1.522 1.512h9.13l1.522-1.512H71v4.537A3.033 3.033 0 0 1 67.957 63z"></path>
                            <path d="M39.043 22.17h24.348a3.033 3.033 0 0 1 3.044 3.025V55.44M17.74 55.439V25.195a3.033 3.033 0 0 1 3.043-3.024h9.13M14.696 41.83H4.043A3.033 3.033 0 0 1 1 38.804v-4.537h13.696"></path>
                            <path d="M5.565 34.268V4.024A3.033 3.033 0 0 1 8.61 1h42.608a3.033 3.033 0 0 1 3.044 3.024v15.122"></path>
                            <path d="M39.043 16.122l-4.565-4.537-4.565 4.537M39.043 28.22l-4.565 4.536-4.565-4.536M34.478 11.585v21.171"></path>
                        </g>
                    </svg>
                </div>
                <h3 class="design-title">Web design services</h3>
            </li>
        </ul>
    </div>
</section>



<div class="vi-page-wrapper">
    <div class="services-section">
        <div class="vi-container">
            <h2>Services</h2>
            <h5>Visions comprises of a team of experienced software professionals dedicated to provide a reliable and cost-effective product. </h5>
            <ul class="vi-clear-fix id-icon">
                <li>
                    <a href="#s1">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/web-designing-icon.png" alt="web-designing-icon" title="Web Designing">
                        <span>Web Designing</span>
                    </a>
                </li>
                <li>
                    <a href="#s2">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/graphic-designing-icon.png" alt="graphic-designing-icon" title="Graphic Designing">
                        <span>Graphic Designing</span>
                    </a>
                </li>
                <li>
                    <a href="#s3">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/application-development-icon.png" alt="application-development-icon" title="Application Development">
                        <span>Application Development</span>
                    </a>
                </li>
                <li>
                    <a href="#s4">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/android-app-development-icon.png" alt="android-app-development-icon" title="Android Apps Development">
                        <span>Android Apps Development</span>
                    </a>
                </li>
                <li>
                    <a href="#s5">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/iphone-app-development-icon.png" alt="iphone-app-development-icon" title="Iphone Apps Development">
                        <span>Iphone Apps Development</span>
                    </a>
                </li>
                <li>
                    <a href="#s6">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/offshore-outsourcing-icon.png" alt="offshore-outsourcing-icon" title="Offshore Outsourcing">
                        <span>Offshore Outsourcing</span>
                    </a>
                </li>
                <li>
                    <a href="#s7">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/network-administration-icon.png" alt="network-administration-icon" title="Network Administration">
                        <span>Network Administration</span>
                    </a>
                </li>
                <li>
                    <a href="#s8">
                        <img src="https://www.visions.net.in/sites/all/themes/visions/images/services_files/seo-services-icon.png" alt="seo-services-icon" title="Seo Services">
                        <span>Seo Services</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>




<section class="o-hidden pt-0" style="background-color: #f9f9f9;">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-8 col-md-12 ml-auto mr-auto">
                <div class="section-title">
                    <h2>Service</h2>
                    <h5 class="title">We build powerful digital solutions &amp; experiences</h5>
                </div>
            </div>
        </div>
        <div class="row hk_colors">
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/graphic-designing">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288388_EmQL7ydP7u1Eld0_Service 5.png" alt="Graphic Designing" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Graphic Designing</h5>
                        </div>
                        <div class="featured-desc">
                            <p>We Consider Designing, an essential identity fabric of any business, that’s why we…</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/website-designing">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574085793_psz4v6B46bmHa3R_web design.png" alt="Website Designing" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Website Designing</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Our website design, development, and conversion optimization facilities are focused on...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/website-development">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288406_j1dB2splUO2xo09_Service 7.png" alt="Website Development" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Website Development</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Everyone knows a good web development company will be one with open contact and thoug...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/website-maintenance">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574085742_ZqqONgPJaUj8oUg_Web maintenance.png" alt="Website Maintenance &amp; Support" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Website Maintenance &amp; Support</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Maintenance is important for any website. Once the website is set up, it needs updating, monitoring,...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/mobile-application-development">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288358_qHGgKqOAfWR6w76_Service 4.png" alt="Mobile Application Development" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Mobile Application Development</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Gexton has a big team that is an expert in mobile app development. Our mobile app developers are exc...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/security-solutions">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288332_1IwCw2NjIl3j448_Service 3.png" alt="Security Solutions" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Security Solutions</h5>
                        </div>
                        <div class="featured-desc">
                            <p>The Gexton having its roots and acclaimed legacy in most sustainable technology implementations...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/content-management-system-cms">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574085681_yEDDTjgv3yFof18_content managment.png" alt="Content Management System (CMS)" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Content Management System (CMS)</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Content Management Systems, or CMS, is a great solution for setting up and maintaining...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/domain-and-web-hosting">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288284_8y9wgsJFxYIduX5_Service 1.png" alt="Domain and Web Hosting" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Domain and Web Hosting</h5>
                        </div>
                        <div class="featured-desc">
                            <p>GEXTON is the premier registrar of domain names, providing and domains as well as offers...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/accounting-system">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574085499_lpeJzOGt01M4zGr_Account stsystem.png" alt="Accounting System" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Accounting System</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Choosing accounting software can be confusing. Eliminate the confusion...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/seo">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288309_khKihxdcoo9wPlZ_Service 2.png" alt="Digital Marketing SEO &amp; SEM" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Digital Marketing SEO &amp; SEM</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Search Engines do four things – crawling, building an index, calculating relevancy &amp; rankings...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/attendance-management-system">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574085385_8QPB7yuuP9T66Ag_attendance.png" alt="Attendance Management System" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Attendance Management System</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Maintenance and the consistent upkeep of the employee's attendance management system...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/gps-tracking">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574084837_Ny4CPsFqy1x2wuV_gps tracking.png" alt="GPS Tracking" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>GPS Tracking</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Gexton Inc. Pakistan with the successful record of implementing security based and IT support...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/offshore-development">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574085120_KAiUGHWfb2zo0E4_offshore.png" alt="Offshore Development" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Offshore Development</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Outsourcing is a rapidly growing practice in this economy because of companies...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/print-media-branding">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574084979_NE17RCekbNbicte_Print media.png" alt="Print Media &amp; Branding" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Print Media &amp; Branding</h5>
                        </div>
                        <div class="featured-desc">
                            <p>GEXTON is presenting great services of Print Media &amp; Branding. We provide our customers...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/sms-marketing">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574083736_n0PEnsVkRgFRRAc_sms marketing.png" alt="SMS marketing" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>SMS marketing</h5>
                        </div>
                        <div class="featured-desc">
                            <p>The product marketing trends have been under huge transformation through last decades, but their...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/software-development">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574083708_e0tBdRBRL4YBq6K_Software development.png" alt="Software Development" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Software Development</h5>
                        </div>
                        <div class="featured-desc">
                            <p>applications are the Glue that holds the organization together...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/promotional-voice-calls">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2019/full_1574083630_3l3xIOy7vSzQ0Cr_voice call.png" alt="Promotional Voice Calls" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>Promotional Voice Calls</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Cold Calling has been under use by the advertising agencies across the world...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/app-store-optimization-aso">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1582288461_DbYoRW6bL7NJtuL_Service 6.png" alt="APP STORE OPTIMIZATION (ASO)" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>APP STORE OPTIMIZATION (ASO)</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Gexton is the best software company providing with successful App Store Optimization (ASO)...</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                <a href="https://gexton.com/social-media-marketing">
                    <div class="featured-item custom_featured_services text-center style-2">
                        <div class="featured-icon">
                            <img class="img-center" src="https://gexton.com/uploads/services_icons/2020/full_1584432298_57Z0yK6u4sMvAbY_social media-01.png" alt="social media marketing" height="115">
                        </div>
                        <div class="featured-title">
                            <h5>social media marketing</h5>
                        </div>
                        <div class="featured-desc">
                            <p>Social media marketing refers to the procedure of fast traffic or awareness through social media sit...</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>








<section class="service-why">
    <h2 class="service-why__title service-why__title--team-extension h1">
        OUR ENTERPRISE SOFTWARE SOLUTIONS
    </h2>
    <p class="service-why__description">
        Your business needs a reliable enterprise software solution to support your many operations. It needs to scale, coordinate other software and mobile apps, incorporate advance tech, and cover an extensive list of other objectives. Intellectsoft has the experience, tools, and technologies to create such demanding custom software.
    </p>
    <section class="service-why__points">
        <div class="service-why__point-block" style="background-image: url('resources/assets/images/services/mApp.png')">
            <div class="service-why__mask"></div>
            <h3 class="service-why__point-title">
                Custom Enterprise Software Development
            </h3>
            <div class="service-why__point-lead-content">
                <p class="service-why__point-lead">
                    Support your business infrastructure with scalable software that improves key facets of your enterprise, from automation to employee collaboration. Leverage our decade-long expertise to build an enterprise software solution that solves a particular business objective in one of your departments.
                </p>
            </div>
        </div>
        <div class="service-why__point-block" style="background-image: url('resources/assets/images/MobileApps.png')">
            <div class="service-why__mask"></div>
            <h3 class="service-why__point-title">
                Software Integration
            </h3>
            <div class="service-why__point-lead-content">
                <p class="service-why__point-lead">
                    Enterprises like yours rely on well-coordinated SaaS and on-premise third-party applications. Improve your software infrastructure with well-built microservices, reliable API, and data integration. Launch your mobile strategy with our comprehensive enterprise application development, or use our enterprise app integration solutions.
                </p>
            </div>
        </div>
        <div class="service-why__point-block" style="background-image: url('resources/assets/images/NotUse/download.jpg')">
            <div class="service-why__mask"></div>
            <h3 class="service-why__point-title">
                Legacy Application Modernization
            </h3>
            <div class="service-why__point-lead-content">
                <p class="service-why__point-lead">
                    Outdated enterprise applications often cause employees to spend more time working around limitations. Hire our team to perform an in-depth feature and technical analysis of your legacy solution and improve it with the latest tools and technologies. A well-built legacy solution will help your employees become more productive and satisfied.
                </p>
            </div>
        </div>
        <div class="service-why__point-block" style="background-image: url('https://www.intellectsoft.net/assets/da6d2af7/img/services/product-engeneering/custom_mini_mini.jpg')">
            <div class="service-why__mask"></div>
            <h3 class="service-why__point-title">
                Application Portfolio Consolidation
            </h3>
            <div class="service-why__point-lead-content">
                <p class="service-why__point-lead">
                    Frequent mergers and acquisitions leave enterprises with a wide variety of uncoordinated software solutions. Our experts will help you consolidate, integrate, and refactor your enterprise software as necessary, moving them to modern software stacks and cloud hosting. You will streamline IT operations, minimize costs, and increase employee efficiency.
                </p>
            </div>
        </div>
    </section>
</section>

@endsection