@extends('layouts.app')

@section('content')

<style>

</style>

<!-- 
<div class="page-heading wow fadeIn" data-wow-duration="0.5s">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading-content-bg wow fadeIn" data-wow-delay="0.75s" data-wow-duration="1s">
					<div class="row">
						<div class="heading-content col-md-12">
							<p><a href="index.html">Homepage</a> / <em> Cars</em> / <em> Car Details</em></p>
							<h2>Car <em>Details</em></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->


<div id="myCarousel" class="carousel slide carousel-fade backgroundCustom" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>Strong and Expereinced <br>ASP.NET Team.</h4>
                            <p>Best ASP.NET Development Company India.
                                Hire Best ASP.NET Developers.</p>
                            <a href="{{'web_application'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\dot-net-services.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>PHP Development<br> Company India</h4>
                            <p>
                                Build Custom & Dynamic web Apps with our PHP Developers.
                                Hire PHP developers India. <br>
                            </p>
                            <a href="{{'web_application'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\phpServicesSl.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12 order-md-1 text-center order-2">
                            <h4>Android App Development <br>Company India.</h4>
                            <p>
                                Build Android apps as per your need with our Android App Developers.
                                <br>
                                Hire Android app developers India. 
                            </p>
                            <a href="{{'mobile_applications'}}">Learn More</a>
                        </div>
                        <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\MobileApps.png" class="mx-auto" alt="slide"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
</div>
<!--slide end-->





















<section class="amazing-dashboard-block  no-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7  col-sm-12 wow wdth_100 slideInLeft" style="visibility: visible;">
				<div class="col-md-12">
					<div class="">
						<br>
						<br>
						<div class="text ">
							<h6>WEB DESIGN</h6>
							<h3>Empowering Your Web Identity</h3>
							<p>Web designing is the visual attraction and the page layout of a website that goes along with web development. It is important for brand building as it gives a strong impression.
								<br>
								<br>We at MGL have a team of web development experts who are continuously exploring and executing creative ideas that will change the design of your website into a treasure house for your business.
								<br>
								<br>Design is the foremost element that people observe while visiting your website and if it is not appealing they will exit. It is worth spending your money on creating a well-designed website as it is a long term investment for your business.</p>
							<div class="clearfix"></div>
							<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 wdth_100 no-padding wow slideInRight" style="visibility: visible;">
				<img class="img-responsive mobile_none" src="resources/assets/images/services/WebDevSe1.gif" alt="MGE Web Development">
			</div>
		</div>
	</div>
</section>






<div class="recent-car single-service wow fadeIn" data-wow-delay="0.5s" data-wow-duration="1s">
	<div class="container-fluid">
		<div class="recent-services-content">
			<div class="row">
				<div class="col-md-6">
					<div id="single-service" class="slider-pro">
						<div class="sp-slides">

							<div class="sp-slide">
								<img class="sp-image" src="resources\assets\images\services\web_dev.png" alt="" />
							</div>

							<div class="sp-slide">
								<img class="sp-image" src="resources\assets\images\services\web_dev1.jpg" alt="" />
							</div>

							<div class="sp-slide">
								<img class="sp-image" src="resources\assets\images\services\web_dev2.png" alt="" />
							</div>

							<div class="sp-slide">
								<img class="sp-image" src="resources\assets\images\services\web_dev3.jpeg" alt="" />
							</div>

							<div class="sp-slide">
								<img class="sp-image" src="resources\assets\images\services\web_dev4.jpg" alt="" />
							</div>

						</div>

						<div class="sp-thumbnails">
							<img class="sp-thumbnail" src="resources\assets\images\services\web_dev.png" alt="" />
							<img class="sp-thumbnail" src="resources\assets\images\services\web_dev1.jpg" alt="" />
							<img class="sp-thumbnail" src="resources\assets\images\services\web_dev2.png" alt="" />
							<img class="sp-thumbnail" src="resources\assets\images\services\web_dev3.jpeg" alt="" />
							<img class="sp-thumbnail" src="resources\assets\images\services\web_dev4.jpg" alt="" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="service-details">
						<h4>Web Applications</h4>
						<span>In today's digital market place, a mere static online presence for your business is not enough.</span>

						<p>
							Organizations, the world over are working towards establishing complete online systems on
							which businesses can run efficiently. As internet access becomes more widespread and businesses
							more global, a solid web strategy to connect with your customers, vendors and partners are vital to
							any growth oriented business or organization.
						</p>

						<p>
							We provide professional web application development services to serve the unique needs of your business. Starting with
							brochure sites to multi-functional web portals, we help you move ahead of your competition using the latest technologies and
							industry trends. We have proven expertise in building applications to serve your internal needs as well as applications which
							can open new avenues and capture new markets for your usiness. Focus on ultimate user experience, professional style,
							performance and scalability are the hallmark of our web application initiatives. Hosted on cloud servers or at your corporate
							data centre, our applications are designed to handle extreme load and traffic conditions.
						</p>
						<!-- <div class="container">
								<div class="row">
									<ul class="services-info-1 col-md-6">
										<li><i class="fa fa-check" aria-hidden="true"></i><p>2016/2017</p></li>
										<li><i class="flaticon flaticon-speed"></i><p>240p/h</p></li>
										<li><i class="flaticon flaticon-road"></i><p>20.000km - 40.000km</p></li>
										<li><i class="flaticon flaticon-fuel"></i><p>Diesel</p></li>
									</ul>
									<ul class="services-info-1 col-md-6">
										<li><i class="flaticon flaticon-art"></i><p>White</p></li>
										<li><i class="flaticon flaticon-shift"></i><p>Automatic</p></li>
										<li><i class="flaticon flaticon-car"></i><p>4/5</p></li>
										<li><i class="flaticon flaticon-motor"></i><p>3000cm3</p></li>
									</ul>
								</div>
							</div> -->
						<div class="similar-info">
							<div class="primary-button">
								<a href="#">Add Offer <i class="fa fa-dollar"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section>
	<div class="more-details">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="item wow fadeInUp" data-wow-duration="0.5s">
						<div class="sep-section-heading">
							<h2>Creating an <em>online store</em></h2>
						</div>
						<p>
							Whether you have a physical store or not, an online store can be a great cost effective method of capturing a
							wider market for your business. We provide full cycle services for online store development right from adding
							products your online store to accepting payments electronically and tracking your inventory.
						</p>
						<p>
							Our expertise spans B2C and B2B ecommerce portals development, integrated ERP to automate your back end administration, payment
							gateway and mobile device integration.
						</p>
					</div>

				</div>
				<div class="col-md-4">
					<div class="item wow fadeInUp" data-wow-duration="0.75s">
						<div class="sep-section-heading">
							<h2>Lead generation <em>& Marketing</em></h2>
						</div>
						<p>The web is the ultimate source of lead generation. We can help you attract more prospects to your website and convert the prospects to customers using innovative applications.
						<br>
						The Offsite Development Centre (ODC) comprises of a Delexcent facility and staff dedicated for use for a partner�s project requirements.
						We analyze our client's requirements thoroughly in order to prevent further changes to the software requirements specification and develop an exact plan at a very detailed level.
					</p>
					</div>
				</div>
				<div class="col-md-4 wow fadeInUp" data-wow-duration="1s">
					<div class="item">
						<div class="sep-section-heading">
							<h2>Collabo <em>ration</em></h2>
						</div>
						<p>
							By extending your online systems to your suppliers and vendors you can improve efficiency of your supply chain and product
							delivery network. Improving collaboration across your organization and partner network would help to avoid rework and redundancy
							at all levels, contributing to boost your bottom-line.
						</p>
						<p>
							We offer Enterprise Information Portals, Content Distribution Portals and Business Intelligence Portal solutions to help you connect better with all stake holders in your business ecosystem.
						</p>
						<!-- <div class="contact-info">
								<div class="row">
									<div class="phone col-md-12 col-sm-6 col-xs-6">
										<i class="fa fa-phone"></i><span>+1 123 489-5748</span>
									</div>
									<div class="mail col-md-12 col-sm-6 col-xs-6">
										<i class="fa fa-envelope"></i><span>youremail@gmail.com</span>
									</div>
								</div>
							</div> -->
					</div>
				</div>


				<div class="col-md-12">
					<div class="item wow fadeInUp" data-wow-duration="0.75s">
						<div class="sep-section-heading">
							<h2>Facebook <em>Applications</em></h2>
						</div>
						<p>
							We have comprehensive expertise in developing applications for facebook and other social networks.
							The shift from viewing the web as an information store to the interactive web as we have now makes your
							online social identity as important as your website. We can help you make applications for the social media
							to convert your leads to customers and increase your reach and accessibility to a global marketplace.

						</p>
					</div>
				</div>


			</div>
		</div>
	</div>
</section>


@endsection