@extends('layouts.app')

@section('content')

<div class="Modern-Slider">
	<!-- Slide 1 -->
	<div class="item">
		<div class="img-fill">
			<img src="resources/assets/images/Slider/1.jpg" alt="">
			<div class="info">
				<div>
					<h5>Welcome to MGL (Pvt) Ltd </h5>
					<h3>We globally focused IT solutions and <em>services</em>...</h3>
					<h6 class="secondary-button">
						<a href="#">Contact Us <i class="fas fa-arrow-right"></i></a>
					</h6>
				</div>
			</div>
		</div>
	</div>
	<!-- // Slide 1 -->
	<!-- Slide 2 -->
	<div class="item">
		<div class="img-fill">
			<img src="resources/assets/images/Slider/2.jpg" alt="">
			<div class="info">
				<div>
					<h5>Enterprise Business Applications</h5>
					<h3>Our Enterprise Application competency spanning SAP, Oracle,Microsoft and open source <em>platforms</em>?</h3>
					<h6 class="secondary-button">
						<a href="#">Contact Us <i class="fas fa-arrow-right"></i></a>

					</h6>
				</div>
			</div>
		</div>
	</div>
	<!-- // Slide 2 -->
	<!-- Slide 3 -->
	<div class="item">
		<div class="img-fill">
			<img src="resources/assets/images/Slider/4.png" alt="">
			<div class="info">
				<div>
					<h5>Grow your business with open source</h5>
					<h3>To get more out of less is the mantra of <em>success today.</em>?</h3>
					<h6 class="secondary-button">
						<a href="#">Contact Us <i class="fas fa-arrow-right"></i></a>

					</h6>
				</div>
			</div>
		</div>
	</div>
	<!-- // Slide 3 -->
</div>





<section class="top-slider-features wow fadeIn" data-wow-duration="1.5s">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="slider-top-features" style="background-color: unset; margin-top: 146px;">
					<div id="owl-top-features" class="owl-carousel owl-theme">
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/dotnetcore.png" alt=""></a>
							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>ASP.Net Core</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/SEO.png" alt=""></a>
							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>SEO</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/Android.png" alt=""></a>

							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>Android Application</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/wcf.png" alt=""></a>
							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>WCF Development</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/reactJs.jpg" alt=""></a>
							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>React Js Development</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/nodejs.jpg" alt=""></a>

							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>Node.js Development</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/Angular.jpg" alt=""></a>
							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>Angular Development</h4>
								</a>
								<span>TEXT</span>
							</div> -->
						</div>
						<div class="item services-item-1">
							<div class="thumb-content">
								<a href="{{'services'}}"><img src="resources/assets/images/services/php1.jpg" alt=""></a>
							</div>
							<!-- <div class="down-content">
								<a href="{{'services'}}">
									<h4>PHP Development</h4>
								</a>
								<span>Text</span>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>






<section class="amazing-dashboard-block  no-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7  col-sm-12 wow wdth_100 slideInLeft" style="visibility: visible;">
				<div class="col-md-12">
					<div class="">
						<br>
						<br>
						<div class="text ">
							<h6>About MGL</h6>
							<h3>We are a Software Development Company &amp; We Love Solving Business Problems</h3>
							<p>
								MGL offers multiple channels for outsourced software development based on unique requirements of our clients/partners.
								MGL is software development and web development Company based in India. we are providing IT solutions and IT-enabled services. Ever since our establishment, we are growing every day to meet the growing demands of our clients and surpass their expectations.
								<br>
								<br>Our expertise in providing industry-specific services and custom solutions has enabled us to deliver absolute business solutions to our worldwide clients. We are providing our services to small as well as big corporate houses including individual entrepreneurs using our unmatched expertise and cutting edge technology.</p>
							<div class="clearfix"></div>
							<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 wdth_100 no-padding wow slideInRight" style="visibility: visible;">
				<img style="width: 100%;" class="img-responsive mobile_none" src="resources/assets/images/about.jpg" alt="">
			</div>
		</div>
	</div>
</section>









<div class="container card shadow d-flex justify-content-center mt-5">
	<!-- nav options -->
	<ul class="nav nav-pills justify-content-center mb-3 " id="pills-tab" role="tablist">
		<li class="nav-item"> <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Variable</a> </li>
		<li class="nav-item"> <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Operators</a> </li>
		<li class="nav-item"> <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Values</a> </li>
	</ul> <!-- content -->
	<div class="tab-content" id="pills-tabContent p-3">
		<!-- 1st card -->
		<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
			<div id="services" class="offers-tabcontent active">
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="85" height="82" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" filter="url(#a)" transform="translate(-385 -47)">
							<path stroke="#323232" d="M451.478 110h-46.956c-1.945 0-3.522-1.562-3.522-3.488v-2.326h21.13l1.174 1.163h9.392l1.174-1.163H455v2.326c0 1.926-1.577 3.488-3.522 3.488zm-46.956-5.814v-3.488 3.488zm21.13-31.395h23.478a2.336 2.336 0 012.348 2.325v29.07"></path>
							<path stroke="#1476F2" d="M423.304 86.744v9.303a2.336 2.336 0 01-2.347 2.325h-17.61A2.336 2.336 0 01401 96.047V62.326A2.336 2.336 0 01403.348 60h17.609a2.336 2.336 0 012.347 2.326v17.441"></path>
							<path stroke="#1476F2" stroke-linejoin="round" d="M401 91.395h22.304m0-25.581H401m9.391 29.07h3.522"></path>
							<path stroke="#1476F2" d="M433.87 83.256h-21.13m3.52 3.488l-3.52-3.488 3.52-3.489m14.088 6.977l3.522-3.488-3.522-3.489"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Enterprise Software Development </h3>
						<p class="offer-content-description">
							Create complex enterprise software, ensure reliable software integration, modernise your legacy system. </p>
						<a href="/services/enterprise-development" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="85" height="71" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" filter="url(#a)" transform="translate(-754 -39)">
							<path stroke="#494946" d="M798.174 74.455v7.09a2.355 2.355 0 002.348 2.364h16.435a2.355 2.355 0 002.347-2.364v-20.09a2.355 2.355 0 00-2.347-2.364h-16.435a2.355 2.355 0 00-2.348 2.364v5.909m3.522 16.545V91h14.087v-7.09m0-24.82V52h-14.087v7.09"></path>
							<path stroke="#494946" d="M808.74 70.91h-28.175m3.522 3.545l-3.522-3.546 3.522-3.545m21.13 7.09l3.522-3.545-3.522-3.545m16.435 0h1.174c.648 0 1.174.53 1.174 1.181v4.728c0 .652-.526 1.182-1.174 1.182h-1.174"></path>
							<path stroke="#1476F2" stroke-linejoin="round" d="M770 83.91h22.304m0-26H770m9.391 29.545h3.522"></path>
							<path stroke="#1476F2" d="M792.304 74.455v14.181A2.355 2.355 0 01789.957 91h-17.61A2.355 2.355 0 01770 88.636V54.364A2.355 2.355 0 01772.348 52h17.609a2.355 2.355 0 012.347 2.364v13"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Mobile App Development </h3>
						<p class="offer-content-description">
							Create an impactful mobile app that fits your brand and industry within a shorter time frame. </p>
						<a href="/services/mobile-development" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="77" height="81" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" stroke-linejoin="round" filter="url(#a)" transform="translate(-7 -223)">
							<path stroke="#323232" d="M45.5 232v10.34V232zm-12.75 3.386l5.216 8.956-5.216-8.956zm-9.334 9.252l9.035 5.17-9.035-5.17zM20 257.277h10.432H20zm3.416 12.638l9.035-5.17-9.035 5.17zm44.168 0l-9.035-5.17 9.035 5.17zM71 257.277H60.568 71zm-3.416-12.639l-9.035 5.17 9.035-5.17zm-9.334-9.252l-5.216 8.956 5.216-8.956z"></path>
							<path stroke="#1476F2" d="M38.545 275.66h13.91m-13.91 4.595h13.91m-13.91-22.978c0-3.808 3.114-6.894 6.955-6.894"></path>
							<path stroke="#323232" d="M42.023 260.723l3.477 3.447 3.477-3.447M45.5 264.17v8.043"></path>
							<path stroke="#1476F2" d="M50.136 284.851h-1.159L47.818 286h-4.636l-1.16-1.149h-1.158m10.431-12.638v-1.15c0-2.074.728-3.987 2.182-5.458 2.19-2.214 3.614-4.953 3.614-8.328 0-6.346-5.19-11.49-11.591-11.49-6.402 0-11.59 5.144-11.59 11.49 0 3.375 1.424 6.114 3.613 8.328 1.454 1.47 2.182 3.384 2.182 5.459v1.149"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							IT Consulting </h3>
						<p class="offer-content-description">
							Trust our top minds to eliminate workflow pain points, implement new tech, and consolidate app portfolios. </p>
						<a href="/services/it-consulting-services" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="85" height="82" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" stroke-linejoin="round" filter="url(#a)" transform="translate(-9 -39)">
							<path stroke="#323232" d="M73.182 100.2c2.343 0 4.818-1.266 4.818-3.6V85.8c0-3.964-7.075-6-10.84-6-.373 0-.779.02-1.205.06M73.182 105V88.2 105zm-4.818-28.8c2.807 0 4.818-1.764 4.818-4.32v-2.16c0-2.556-2.01-4.32-4.818-4.32s-4.819 1.764-4.819 4.32v2.16c0 2.556 2.01 4.32 4.819 4.32zm-38.546 24c-2.343 0-4.818-1.266-4.818-3.6V85.8c0-3.964 7.075-6 10.84-6 .373 0 .779.02 1.205.06M29.818 105V88.2 105zm4.818-28.8c-2.807 0-4.818-1.764-4.818-4.32v-2.16c0-2.556 2.01-4.32 4.818-4.32s4.819 1.764 4.819 4.32v2.16c0 2.556-2.01 4.32-4.819 4.32zm6.023-14.4c-2.808 0-4.818-1.764-4.818-4.32v-2.16c0-2.556 2.01-4.32 4.818-4.32s4.818 1.764 4.818 4.32v2.16c0 2.556-2.01 4.32-4.818 4.32zm21.682 0c-2.808 0-4.818-1.764-4.818-4.32v-2.16c0-2.556 2.01-4.32 4.818-4.32s4.818 1.764 4.818 4.32v2.16c0 2.556-2.01 4.32-4.818 4.32z"></path>
							<path stroke="#1476F2" d="M58.727 97.8c2.603 0 4.818-2.466 4.818-4.8V82.2c0-3.964-7.86-6-12.045-6s-12.045 2.036-12.045 6V93c0 2.334 2.215 4.8 4.818 4.8M51.5 72.6c2.808 0 4.818-1.764 4.818-4.32v-2.16c0-2.556-2.01-4.32-4.818-4.32s-4.818 1.764-4.818 4.32v2.16c0 2.556 2.01 4.32 4.818 4.32zm7.227 32.4V84.6 105zM44.273 84.6V105 84.6z"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Dedicated Development Team </h3>
						<p class="offer-content-description">
							Hire a loyal software development team with niche skills and deep expertise. </p>
						<a href="/services/dedicated-development-team" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Layer_1" x="0" y="0" viewBox="0 0 85 84" xml:space="preserve" class="offer-image replaced-svg">
						<style>
							.st0 {
								fill: none;
								stroke: #505050
							}
						</style>
						<path class="st0" d="M49.2 67.2l-4.7 4.6 4.7 4.7"></path>
						<path class="st0" d="M44.5 71.8h26.3c2.6 0 4.6-2.1 4.6-4.7V39.2M36.8 14.4l4.7-4.6-4.7-4.7"></path>
						<path class="st0" d="M41.5 9.8H15.2c-2.6 0-4.6 2.1-4.6 4.7v27.9"></path>
						<path d="M19.8 61h46.4V20.6H19.8zm4.7-35.7h3.1m1.5 0h3.1m-12.4 4.6h46.4" stroke="#1476f2" fill="none"></path>
						<path class="st0" d="M26 54.8h15.5V36.1H26zm20.1-18.7h15.5m-15.5 6.3h15.5m-15.5 6.2h9.3m20.1-12.5V33m0-3.1v-3.1m-65 28v-3.1m0-3.1v-3.1"></path>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							UX / UI Design </h3>
						<p class="offer-content-description">
							Build the product you need on time with an experienced team that uses a clear and effective design process. </p>
						<a href="/services/ui-ux-design-services" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="84" height="72" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" filter="url(#a)" transform="translate(-755 -218)">
							<path stroke="#323232" d="M824 263.941h-21.13l-2.348 2.353h-5.87l-3.522-2.353H770v2.353a4.701 4.701 0 004.696 4.706h44.608a4.701 4.701 0 004.696-4.706v-2.353z"></path>
							<path stroke="#323232" d="M820.478 263.941v-30.588A2.35 2.35 0 00818.13 231h-42.26a2.35 2.35 0 00-2.348 2.353v30.588"></path>
							<path stroke="#1476F2" d="M782.913 239.235l2.348 2.353 4.696-4.706m-7.044 10.588l2.348 2.354 4.696-4.706m-7.044 10.588l2.348 2.353 4.696-4.706m3.521-14.118h16.435m-16.435 8.235h16.435m-16.435 8.236h16.435"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							QA &amp; Testing </h3>
						<p class="offer-content-description">
							Turn to our experts to perform comprehensive, multi-stage testing and auditing of your software. </p>
						<a href="/services/qa-and-software-testing-services" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
			</div>


		</div>
		<!-- 2nd card -->
		<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
			<div id="technologies" class="offers-tabcontent active">
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="84" height="78" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" filter="url(#a)" transform="translate(-3 -220)">
							<path stroke="#323232" d="M59.087 263.657h4.696c4.538 0 8.217-3.674 8.217-8.207 0-4.533-3.679-8.208-8.217-8.208-.872 0-1.712.138-2.5.39a8.253 8.253 0 00.152-1.563c0-4.533-3.68-8.208-8.217-8.208a8.183 8.183 0 00-5.368 2.002c-1.6-4.575-5.948-7.863-11.074-7.863-6.484 0-11.74 5.25-11.74 11.727 0 1.237.193 2.427.549 3.547-4.242.324-7.585 3.856-7.585 8.176 0 4.531 3.68 8.207 8.218 8.207h3.521"></path>
							<path stroke="#323232" d="M25.584 247.266a8.196 8.196 0 014.974 1.213m17.312-8.624c.301.857.504 1.76.597 2.695m12.819 5.082a8.176 8.176 0 01-1.025 2.668"></path>
							<path stroke="#3F7FE7" d="M53.218 278.9h-17.61a3.52 3.52 0 01-3.52-3.518v-1.172a3.52 3.52 0 013.52-3.518h17.61a3.52 3.52 0 013.521 3.518v1.172a3.52 3.52 0 01-3.521 3.518z"></path>
							<path stroke="#3F7FE7" d="M53.218 270.692h-17.61a3.52 3.52 0 01-3.52-3.517v-1.173a3.52 3.52 0 013.52-3.517h17.61a3.52 3.52 0 013.521 3.517v1.173a3.52 3.52 0 01-3.521 3.517z"></path>
							<path stroke="#3F7FE7" d="M53.218 262.485h-17.61a3.52 3.52 0 01-3.52-3.518v-1.172a3.52 3.52 0 013.52-3.518h17.61a3.52 3.52 0 013.521 3.518v1.172a3.52 3.52 0 01-3.521 3.518z"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Cloud Computing </h3>
						<p class="offer-content-description">
							Use cloud computing solutions to create a scalable, flexible, and connected enterprise environment. </p>
						<a href="/technologies/cloud-computing-solutions" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="84" height="81" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" filter="url(#a)" transform="translate(-4 -40)">
							<path stroke="#1476F2" d="M50.696 98.256c0 2.62-2.103 4.744-4.696 4.744-2.593 0-4.696-2.124-4.696-4.744 0-2.62 2.103-4.744 4.696-4.744 2.593 0 4.696 2.124 4.696 4.744zm0-23.721c0 2.62-2.103 4.744-4.696 4.744-2.593 0-4.696-2.124-4.696-4.744 0-2.62 2.103-4.744 4.696-4.744 2.593 0 4.696 2.124 4.696 4.744z"></path>
							<path stroke="#505050" d="M41.304 56.744c0 2.62-2.102 4.744-4.695 4.744-2.593 0-4.696-2.124-4.696-4.744 0-2.62 2.103-4.744 4.696-4.744 2.593 0 4.695 2.124 4.695 4.744z"></path>
							<path stroke="#1476F2" d="M46 91.14V79.28"></path>
							<path stroke="#505050" d="M42.478 67.419l-3.521-5.93"></path>
							<path stroke="#1476F2" d="M54.217 82.837c0 2.62 2.103 4.744 4.696 4.744 2.593 0 4.696-2.124 4.696-4.744 0-2.62-2.103-4.744-4.696-4.744-2.593 0-4.696 2.124-4.696 4.744z"></path>
							<path stroke="#505050" d="M50.696 56.744c0 2.62 2.102 4.744 4.695 4.744 2.593 0 4.696-2.124 4.696-4.744 0-2.62-2.103-4.744-4.696-4.744-2.593 0-4.695 2.124-4.695 4.744zM63.609 75.72l2.348-3.557"></path>
							<path stroke="#1476F2" d="M51.87 92.326l4.142-5.93"></path>
							<path stroke="#505050" d="M73 67.419c0 2.62-2.102 4.744-4.696 4.744-2.593 0-4.695-2.124-4.695-4.744 0-2.62 2.102-4.745 4.695-4.745C70.898 62.674 73 64.8 73 67.42z"></path>
							<path stroke="#1476F2" d="M37.783 82.837c0 2.62-2.103 4.744-4.696 4.744-2.593 0-4.696-2.124-4.696-4.744 0-2.62 2.103-4.744 4.696-4.744 2.593 0 4.696 2.124 4.696 4.744z"></path>
							<path stroke="#505050" d="M28.391 75.72l-2.348-3.557"></path>
							<path stroke="#1476F2" d="M40.13 92.326l-4.142-5.93"></path>
							<path stroke="#505050" d="M19 67.419c0 2.62 2.102 4.744 4.696 4.744 2.593 0 4.695-2.124 4.695-4.744 0-2.62-2.102-4.745-4.695-4.745C21.102 62.674 19 64.8 19 67.42zm30.522 0l3.521-5.93"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Blockchain Development </h3>
						<p class="offer-content-description">
							Introduce the highest level of security and automate your operations with our blockchain solutions. </p>
						<a href="https://blockchain.intellectsoft.net/" class="offer-content-link-more" onclick="ga('send', 'event', 'blockchain_lab', 'click', 'block_solution')">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="75" height="82" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" filter="url(#a)" transform="translate(-757 -29)">
							<path stroke="#505050" d="M810.699 55.936C810.776 45.843 804.104 41 794.442 41c-9.662 0-16.334 4.843-16.256 14.936H810.7z"></path>
							<path stroke="#3F7FE7" d="M813.842 74.958l-11.707 2.808a6.868 6.868 0 01-3.505-.96l-1.798-1.068a4.687 4.687 0 00-4.78 0l-1.798 1.067a6.86 6.86 0 01-3.503.96l-11.708-2.807a3.454 3.454 0 01-2.663-3.35V59.384c0-1.904 1.56-3.447 3.484-3.447h37.157c1.924 0 3.483 1.543 3.483 3.447V71.61c0 1.59-1.1 2.974-2.662 3.349z"></path>
							<path stroke="#505050" d="M809.537 78.915v1.149c0 2.994-1.313 4.546-3.589 6.519-1.632 1.415-3.576 2.856-5.304 4.11a10.575 10.575 0 01-12.403 0c-1.728-1.254-3.672-2.695-5.305-4.11-2.277-1.973-3.589-3.525-3.589-6.52v-1.148m3.483 8.042V95m23.224-8.043V95"></path>
							<path stroke="#3F7FE7" d="M790.959 59.383h6.967m-22.062 0v10.34m37.156-10.34v10.34"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Augmented Reality Solutions </h3>
						<p class="offer-content-description">
							Build better prototypes, improve the user experience, and lower costs with AR solutions. </p>
						<a href="https://ar.intellectsoft.net/" class="offer-content-link-more" onclick="ga('send', 'event', 'ar_lab', 'click', 'block_solution')">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Layer_1" x="0" y="0" viewBox="0 0 85 84" xml:space="preserve" class="offer-image replaced-svg">
						<style>
							.st0 {
								fill: none;
								stroke: #505050
							}
						</style>
						<path class="st0" d="M49.2 67.2l-4.7 4.6 4.7 4.7"></path>
						<path class="st0" d="M44.5 71.8h26.3c2.6 0 4.6-2.1 4.6-4.7V39.2M36.8 14.4l4.7-4.6-4.7-4.7"></path>
						<path class="st0" d="M41.5 9.8H15.2c-2.6 0-4.6 2.1-4.6 4.7v27.9"></path>
						<path d="M19.8 61h46.4V20.6H19.8zm4.7-35.7h3.1m1.5 0h3.1m-12.4 4.6h46.4" stroke="#1476f2" fill="none"></path>
						<path class="st0" d="M26 54.8h15.5V36.1H26zm20.1-18.7h15.5m-15.5 6.3h15.5m-15.5 6.2h9.3m20.1-12.5V33m0-3.1v-3.1m-65 28v-3.1m0-3.1v-3.1"></path>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							UX / UI Design </h3>
						<p class="offer-content-description">
							Build the product you need on time with an experienced team that uses a clear and effective design process. </p>
						<a href="/services/ui-ux-design-services" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="81" height="84" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" stroke-linejoin="round" filter="url(#a)" transform="translate(-380 -220)">
							<path stroke="#323232" d="M422.045 239.45c0-2.474.217-7.45-6.153-7.45-5.075 0-9.69 5.47-9.843 8.69-3.827.515-7.462 4.287-6.957 9.106-3.637 1.798-6.275 11.402-1.53 15.957-.862 3.917 1.792 12.89 9.717 12.183 1.93 10.462 14.766 10.15 14.766 2.483v-6.208m-7.727-26.011c-4.416 0-8.5-.729-8.5-7.714"></path>
							<path stroke="#3F7FE7" d="M406.59 256.724c2.081 1.324 5.602.8 7.492-1.133 1.396-1.426 2.886-3.69 4.424-3.525h19.767m-22.409 21.591c.915-1.837 3.112-2.314 6.224-2.314h16.185"></path>
							<path stroke="#323232" d="M408.91 270.571c-2.053-.321-3.865-2.253-3.865-3.857m2.319 10.8c0-4.712 2.603-10.8 9.272-10.8"></path>
							<path stroke="#3F7FE7" d="M402.727 252.829c2.134 0 3.864 1.551 3.864 3.743 0 1.73-1.075 3.426-2.575 3.97m25.757-19.285h-7.518c-3.406 0-6.758-.467-7.937-3.086"></path>
							<path stroke="#323232" d="M422.045 244.343v7.714m0 3.086v13.886"></path>
							<path stroke="#3F7FE7" d="M422.045 261.314h7.728m-5.409 20.057H429m17-29.314a3.86 3.86 0 01-3.864 3.857 3.86 3.86 0 01-3.863-3.857 3.86 3.86 0 013.863-3.857 3.86 3.86 0 013.864 3.857zm-9.273-10.028a3.86 3.86 0 01-3.863 3.857 3.86 3.86 0 01-3.864-3.857 3.86 3.86 0 013.864-3.858 3.86 3.86 0 013.863 3.858zm0 20.057a3.86 3.86 0 01-3.863 3.857 3.86 3.86 0 01-3.864-3.857 3.86 3.86 0 013.864-3.857 3.86 3.86 0 013.863 3.857zm0 19.285a3.86 3.86 0 01-3.863 3.858A3.86 3.86 0 01429 281.37a3.86 3.86 0 013.864-3.857 3.86 3.86 0 013.863 3.857zM446 271.343a3.86 3.86 0 01-3.864 3.857 3.86 3.86 0 01-3.863-3.857 3.86 3.86 0 013.863-3.857 3.86 3.86 0 013.864 3.857z"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Artificial Intelligence </h3>
						<p class="offer-content-description">
							Custom AI-based solutions like machine and deep learning will help you automate any operation. </p>
						<a href="https://ai.intellectsoft.net/" class="offer-content-link-more">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="85" height="84" class="offer-image replaced-svg">
						<defs>
							<filter id="a" width="104.3%" height="113.3%" x="-2.1%" y="-5.8%" filterUnits="objectBoundingBox">
								<feOffset dy="3" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
								<feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="7.5"></feGaussianBlur>
								<feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.03 0"></feColorMatrix>
								<feMerge>
									<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
									<feMergeNode in="SourceGraphic"></feMergeNode>
								</feMerge>
							</filter>
						</defs>
						<g fill="none" fill-rule="evenodd" stroke-linejoin="round" filter="url(#a)" transform="translate(-752 -210)">
							<path stroke="#323232" d="M798.14 229.535v-6.28h-10.047v5.682a19.967 19.967 0 00-5.168 2.14l-4.017-4.017-7.104 7.104 4.017 4.017a19.967 19.967 0 00-2.14 5.168H768v10.046h5.681a19.967 19.967 0 002.14 5.168l-4.017 4.017 7.104 7.105 4.017-4.018a19.967 19.967 0 005.168 2.14v5.681h10.047v-6.279"></path>
							<path stroke="#3F7FE7" d="M800.651 234.558h1.256l6.279-6.279m0 41.441l-7.535-7.534h-7.535c-7.629 0-13.814-6.185-13.814-13.814 0-7.629 6.185-13.814 13.814-13.814"></path>
							<path stroke="#3F7FE7" d="M794.372 254.651h-1.256a6.28 6.28 0 010-12.558h10.047l5.023-5.023h6.28m-.001 23.86h-5.023l-6.28-6.279h-1.255m12.558-28.884a3.767 3.767 0 11-7.534 0 3.767 3.767 0 017.534 0z"></path>
							<path stroke="#3F7FE7" d="M822 237.07a3.767 3.767 0 11-7.534 0 3.767 3.767 0 017.534 0zm0 11.302a3.767 3.767 0 11-7.534 0 3.767 3.767 0 017.534 0zm0 12.558a3.767 3.767 0 11-7.534 0 3.767 3.767 0 017.534 0zm-7.535 11.303a3.767 3.767 0 11-7.534 0 3.767 3.767 0 017.534 0zm-12.558-23.861h11.302m-21.349 0h2.512m2.512 0h2.511m-2.511 6.279h2.511m-3.767-20.093h2.512"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Internet of Things </h3>
						<p class="offer-content-description">
							Secure IoT solutions will let you gather Big Data, optimise key processes, and improve decision-making. </p>
						<a href="https://iot.intellectsoft.net/" class="offer-content-link-more" onclick="ga('send', 'event', 'iot_lab', 'click', 'block_solution')">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
			</div>
		</div> <!-- 3nd card -->
		<div class="tab-pane fade third" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
			<div id="industries" class="offers-tabcontent active">
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="39" height="55" class="offer-image replaced-svg">
						<g fill="none" fill-rule="evenodd">
							<path stroke="#494946" stroke-linejoin="round" d="M29.824 44.66V55M9.176 44.66V55m24.089-18.383c0 1.503-.36 4.56-1.147 5.745-.79 1.184-4.589 4.595-4.589 4.595a8.254 8.254 0 01-5.735 2.298h-4.588a8.254 8.254 0 01-5.735-2.298s-3.8-3.41-4.589-4.595c-.789-1.185-1.147-4.242-1.147-5.745M16.06 11.34V1h6.882v10.34M39 23.98H0M12.618 7.894V2.149C7.169 3.968 3.44 7.844 3.44 13.639v4.595l-2.294 1.149v2.298M26.382 7.894V2.149c5.449 1.819 9.177 5.695 9.177 11.49v4.595l2.294 1.149v2.298"></path>
							<path stroke="#1476F2" d="M33.3 26.376h2.21v5.795c-1.59 1.525-2.505 2.313-2.746 2.366-1.705.376-4.206.84-7.502 1.392a5.486 5.486 0 01-2.62-.66l-1.346-.733c-.536-.292-1.185-.808-1.787-.816-.635-.007-1.223.508-1.788.816l-1.345.733a5.48 5.48 0 01-2.62.66c-3.296-.55-5.803-1.014-7.52-1.392-.23-.05-1.195-.839-2.897-2.366v-5.814h2.335"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Construction </h3>
						<p class="offer-content-description">
							Advanced software solutions that take Construction into the Digital Era. </p>
						<a href="/platforms/ar-construction" class="offer-content-link-more" rel="nofollow">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="54" height="45" class="offer-image replaced-svg">
						<g fill="none" fill-rule="evenodd" stroke-linejoin="round">
							<path stroke="#323232" d="M50.033 19.105c.171-.658.305-1.325.404-2.003.61-4.179-.109-9.928-4.495-13.469C41.033-.33 31.495-.353 27 7.285 22.503-.353 12.967-.33 8.057 3.633c-4.385 3.54-5.104 9.29-4.494 13.47a18.46 18.46 0 00.735 3.13m3.752 6.792C11.698 31.746 17.017 38.06 27 44c5.719-7.638 15.844-12.164 19.783-18.105"></path>
							<path stroke="#1476F2" d="M0 23.632h12.375l3.375 2.263 5.625-15.842 2.25 22.631 5.625-12.447 2.25 4.526 3.375-2.263H54"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Healthcare </h3>
						<p class="offer-content-description">
							Patient-friendly software that empowers healthcare industry workers. </p>
						<a href="/platforms/healthcare" class="offer-content-link-more" rel="nofollow">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="40" height="56" class="offer-image replaced-svg">
						<g fill="none" fill-rule="evenodd">
							<path stroke="#323232" d="M35.438 55H4.561C2.595 55 1 53.423 1 51.478V4.522C1 2.577 2.595 1 4.563 1h30.875C37.404 1 39 2.577 39 4.522v46.956C39 53.423 37.405 55 35.437 55zM18.813 4.522h5.937m-10.687 0h2.374M1 47.957h38M1 8.043h38M17.625 51.478h4.75"></path>
							<path stroke="#1476F2" d="M12.875 19.783h19.779c.796 0 1.366.758 1.138 1.51l-2.85 9.392a1.187 1.187 0 01-1.138.837H14.062"></path>
							<path stroke="#1476F2" d="M29.5 36.217H16.437c-.55 0-1.03-.652-1.187-1.174L9.312 17.435h-4.75m9.501 22.305h2.374m10.688 0H29.5"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Retail &amp; eCommerce </h3>
						<p class="offer-content-description">
							Engaging and exciting software solutions for modern retail. </p>
						<a href="/platforms/ecommerce-retail" class="offer-content-link-more" rel="nofollow">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="55" height="50" class="offer-image replaced-svg">
						<g fill="none" fill-rule="evenodd">
							<path stroke="#1476F2" d="M35.892 17h10.34v-4.571l8.031 9.142-8.031 8V25h-5.745M15.21 27.286H9.467v-4.572l-8.043 9.143 8.043 8v-4.571h13.787m5.744 0h-3.447M38.19 25h-3.447m-13.788 2.286H17.51m9.19 0h-3.447M19.806 17h4.17c.952 0 1.723-.768 1.723-1.714 0-.947-.77-1.715-1.723-1.715h-1.149a1.72 1.72 0 01-1.723-1.714 1.72 1.72 0 011.723-1.714H26.7M23.253 17v3.429m0-10.286V6.714"></path>
							<path stroke="#1476F2" d="M33.594 13.571c0-5.68-4.663-10.285-10.415-10.285-5.752 0-10.414 4.604-10.414 10.285 0 5.682 4.662 10.286 10.414 10.286s10.415-4.604 10.415-10.286z"></path>
							<path stroke="#323232" d="M32.71 4.04A4.595 4.595 0 0137.04 1c2.539 0 4.597 2.047 4.597 4.572 0 2.524-2.058 4.571-4.596 4.571a4.59 4.59 0 01-1.151-.145m-3.445 24.145c0-2.525 2.058-4.572 4.596-4.572 2.538 0 4.595 2.047 4.595 4.572 0 2.524-2.057 4.571-4.595 4.571s-4.596-2.047-4.596-4.571zM22.104 44.429c0-2.525 2.058-4.572 4.596-4.572 2.538 0 4.596 2.047 4.596 4.572 0 2.524-2.058 4.571-4.596 4.571-2.538 0-4.596-2.047-4.596-4.571z"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							FinTech </h3>
						<p class="offer-content-description">
							Meet the demands of modern customers in speed and security with scalable financial technology. </p>
						<a href="/solutions/fintech" class="offer-content-link-more" rel="nofollow">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="54" height="56" class="offer-image replaced-svg">
						<g fill="none" fill-rule="evenodd">
							<path stroke="#323232" stroke-linejoin="round" d="M7.043 55v-2.935c0-.972.79-1.76 1.761-1.76h8.805a2.347 2.347 0 002.348-2.348v-.587a1.76 1.76 0 011.76-1.761h.587a2.347 2.347 0 002.348-2.348v-1.174a2.347 2.347 0 00-2.348-2.348h-7.043c-.972 0-1.761.789-1.761 1.761a1.76 1.76 0 01-1.76 1.76H3.521M28.174 55v-3.522a2.347 2.347 0 012.348-2.348h8.217a2.347 2.347 0 100-4.695h-1.174a2.347 2.347 0 110-4.696H48.13"></path>
							<path stroke="#323232" d="M1.174 55c0-14.263 11.563-25.826 25.826-25.826S52.826 40.737 52.826 55H1.174z"></path>
							<path stroke="#1476F2" d="M30.522 8.043l.03-1.173L25.825 1h-3.522l2.348 5.87v1.173zm14.087 4.697a2.347 2.347 0 00-2.348-2.349H15.919c-.128 0-4.18-3.521-4.18-3.521H9.391l1.123 4.714c-.658.347-1.123.695-1.123 1.314 0 1.858 4.164 2.189 5.355 2.189h9.906v2.348l-3.522 7.043h3.522l5.87-5.87 2.267-3.521h9.472a2.347 2.347 0 002.348-2.348z"></path>
							<path stroke="#323232" d="M0 31.774a36.357 36.357 0 0118.783-11.06m15.26-.251a36.348 36.348 0 0119.73 11.061"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Travel &amp; Hospitality </h3>
						<p class="offer-content-description">
							Extend the comfort of your resort with practical software solutions. </p>
						<a href="/platforms/hospitality" class="offer-content-link-more" rel="nofollow">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
				<div class="tabcontent-item">
					<svg xmlns="http://www.w3.org/2000/svg" width="55" height="49" class="offer-image replaced-svg">
						<g fill="none" fill-rule="evenodd" stroke-linejoin="round">
							<path stroke="#323232" stroke-linecap="round" d="M10.756 49v-3.026c0-1.003.82-1.816 1.83-1.816h8.536c1.01 0 1.83-.814 1.83-1.816v-1.21c0-1.003.819-1.816 1.828-1.816H26c1.01 0 1.83-.814 1.83-1.816v-2.421c0-1.002-.82-1.816-1.83-1.816h-8.537c-1.01 0-1.829.814-1.829 1.816 0 1.002-.82 1.816-1.83 1.816H4.66M29.049 49v-2.421a2.43 2.43 0 012.439-2.421h6.097a2.43 2.43 0 002.44-2.421 2.43 2.43 0 00-2.44-2.421h-1.22a2.43 2.43 0 01-2.438-2.421 2.43 2.43 0 012.439-2.421h8.536M1 49c0-13.371 11.193-23 25-23s25 9.629 25 23"></path>
							<path stroke="#1476F2" d="M15 22h23V1H15v21zM30.737 1v4.941h-8.474V1h8.474zm3.631 16.059h-2.42 2.42zm-3.631 0h-2.421 2.42z"></path>
							<path stroke="#323232" stroke-linecap="round" d="M6.893 21.499A35.28 35.28 0 0111.156 19M0 27.644A36.664 36.664 0 013.652 24m46.63.954A36.892 36.892 0 0154 29M41.719 19a35.27 35.27 0 014.89 2.939"></path>
						</g>
					</svg>
					<div class="offer-content">
						<h3 class="offer-content-title">
							Logistics </h3>
						<p class="offer-content-description">
							Use all the latest tech available to make your logistics run like clockwork. </p>
						<a href="/platforms/logistics-and-automotive-software-solutions" class="offer-content-link-more" rel="nofollow">view more<i class="isoi-angle-right"></i></a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>








<!--  Start Home Services  -->
<!-- Horizontal -->
<!-- <div class="container-fluid text-center">
	<div class="col-md-12">
		<div class="flip">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/540518/pexels-photo-540518.jpeg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">MOBILE APP </hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>
		<div class="flip">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/414171/pexels-photo-414171.jpeg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">LAKE</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>
		<div class="flip">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/36717/amazing-animal-beautiful-beautifull.jpg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">OCEAN</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>
		<div class="flip">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/36717/amazing-animal-beautiful-beautifull.jpg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">OCEAN</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>

	</div>


	<div class="col-sm-12">
		<div class="flip flip-vertical">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/38136/pexels-photo-38136.jpeg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">FOREST</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>
		<div class="flip flip-vertical">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/34950/pexels-photo.jpg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">TRACK</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>
		<div class="flip flip-vertical">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/34950/pexels-photo.jpg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">TRACK</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>
		<div class="flip flip-vertical">
			<div class="front" style="background-image: url(https://images.pexels.com/photos/34950/pexels-photo.jpg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb)">
				<h1 class="text-shadow">TRACK</hi>
			</div>
			<div class="back">
				<h2>Angular</h2>
				<p>Good tools make application development quicker and easier to maintain than if you did everything by hand..</p>
			</div>
		</div>


	</div>
</div> -->

<!-- End Home Services  -->




<section class="mt-4">
	<div class="features-search-section">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-6">
							<div class="service-item wow fadeIn" data-wow-duration="0.75s">
								<i class="fab fa-node"></i>
								<div class="text-content">
									<h6>Node.js Development</h6>
									<p>
										We maintain transparency with our clients. Our values and business ethics has given us repeated customers.
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="service-item wow fadeIn" data-wow-duration="0.75s">
								<i class="fab fa-android"></i>
								<div class="text-content">
									<h6>Android Application</h6>
									<p>
										Mobile ApplicationsWith the rapid growth of the smart phone and mobile internet market, the mobile devices have become the primary interface to internet and social networks.
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="service-item wow fadeIn" data-wow-duration="0.75s">
								<i class="fab fa-windows"></i>
								<div class="text-content">
									<h6>WCF Development</h6>
									<p>
										SummationIT provides expert WCF development services. We have delivered multiple projects on WCF and .net development since our inception.
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="service-item wow fadeIn" data-wow-duration="0.75s">
								<i class="fab fa-angular"></i>
								<div class="text-content">
									<h6>Angular Development</h6>
									<p>
										Explore the Customized, Effective and Quality-driven Angular Development Services with the team that know it from the core.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="search-content wow fadeIn" data-wow-duration="0.75s">
						<div class="search-heading">
							<div class="icon">
								<i class="fas fa-envelope-open-text"></i>
							</div>
							<div class="text-content">
								<h2>TALK TO US</h2>
								<span>Thank you for your interest in the services offered by MGL.</span>
							</div>
						</div>
						<form action="">
							<div class="search-form">
								<div class="row">
									<div class="col-md-12">

										<div class="form-group">
											<label for="name" class="text-white">Name</label>
											<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="email" class="text-white">Name</label>
											<input type="email" class="form-control" name="email" id="email" placeholder="Enter Name">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="phone" class="text-white">Phone</label>
											<input type="number" class="form-control" name="phone" id="phone" placeholder="Enter phone">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="services" class="text-white">Services</label>
											<input type="text" class="form-control" name="services" id="services" placeholder="Enter services">
										</div>
									</div>
									<div class="col-md-12">
										<div class="secondary-button">
											<a href="#">Search <i class="fas fa-paper-plane"></i></a>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="mt-4">
	<div class="call-to-action wow fadeIn" data-wow-duration="0.75s">
		<div class="">
			<div class="call-to-action-content">
				<div class="row">
					<div class="col-md-12">
						<p>
							Think Website Call MGL! Looking for a Quick Quote
							<em> regarding your project?</em> which helps you to grow your business easily.</p>
						<div class="secondary-button">
							<a href="{{'contact'}}">Contact Us <i class="fas fa-phone-alt"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="recent-services">
		<div class="container">
			<div class="recent-services-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-heading">
							<!-- <div class="icon">
								<i class="fa fa-car"></i>
							</div> -->
							<div class="text-content">
								<h2>Our Offerings</h2>
								<span>We Focus on Growing your Brands Online</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="services-item-1 wow fadeIn" data-wow-duration="0.75s">
							<div class="thumb-content">
								<!-- <div class="services-banner-1">
									<a href="#">Web Development</a>
								</div> -->
								<div class="thumb-inner">
									<a href="#"><img src="resources\assets\images\services\WebAppHome1.gif" alt=""></a>
								</div>
							</div>
							<div class="down-content">
								<a href="{{'web_application'}}">
									<h4>Web Application Development</h4>
								</a>
								<span>Build Custom Web Application with our Experienced Web Developers.</span>
								<p class="text-justify">
									Our Web application developers are specialized in providing development services, customized web designing, and Ecommerce solutions. Understanding your thoughts and implementing it into any web application is not an easy task.
									We are here to help and execute this for you...
								</p>
								<ul class="services-info-1">
									<a href="{{'mobile_applications'}}">
										<li>
											<div class="item">
												<p>Read More</p>
											</div>
										</li>
									</a>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-item-1 wow fadeIn" data-wow-duration="0.75s">
							<div class="thumb-content">
								<!-- <div class="services-banner-1">
									<a href="#">Web Development</a>
								</div> -->
								<div class="thumb-inner">
									<a href="{{'mobile_applications'}}"><img src="resources\assets\images\services\mobile_deveHome.gif" alt=""></a>
								</div>
							</div>
							<div class="down-content">
								<a href="{{'mobile_applications'}}">
									<h4>Mobile App Development</h4>
								</a>
								<span>Build Mobile apps with our Mobile App Developers.</span>
								<p class="text-justify">
									Mobile Apps have become prevalent due to the increased use of smartphone. Consumer reachability has increased to a new level. Enterprises are now focusing to developing mobile apps for their products and services on every best mobile technology platforms....
								</p>
								<ul class="services-info-1">
									<a href="{{'mobile_applications'}}">
										<li>
											<div class="item">
												<p>Read More</p>
											</div>
										</li>
									</a>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-item-1 wow fadeIn" data-wow-duration="0.75s">
							<div class="thumb-content">
								<!-- <div class="services-banner-1">
									<a href="{{'services'}}">Web Development</a>
								</div> -->
								<div class="thumb-inner">
									<a href="{{'services'}}"><img src="resources\assets\images\services\wpf.jpg" alt=""></a>
								</div>
							</div>
							<div class="down-content">
								<a href="{{'services'}}">
									<h4>WPF Development</h4>
								</a>
								<span>WPF Development Services.
									Hire WPF developers India.</span>
								<p class="text-justify">
									Windows Presentation Foundation (WPF) is Microsoft’s latest approach towards GUI framework. Main motive of WPF is to give the rich look to user interface and with lot more ease for the developers...

								</p>
								<ul class="services-info-1">
									<a href="{{'mobile_applications'}}">
										<li>
											<div class="item">
												<p>Read More</p>
											</div>
										</li>
									</a>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-item-1 wow fadeIn" data-wow-duration="0.75s">
							<div class="thumb-content">
								<!-- <div class="services-banner-1">
									<a href="single_car.html">Web Development</a>
								</div> -->
								<div class="thumb-inner">
									<a href="{{'services'}}"><img src="resources\assets\images\services\database-development.jpg" alt=""></a>
								</div>
							</div>
							<div class="down-content">
								<a href="{{'services'}}">
									<h4>Database Development</h4>
								</a>
								<span>Database Application Development Services</span>
								<p class="text-justify">
									Real-time data managing is very complex and a risky function for the organization which can hamper on its performance. Companies do not like taking risk while accessing the critical information. So it should be left to the experts....
								</p>
								<ul class="services-info-1">
									<a href="{{'mobile_applications'}}">
										<li>
											<div class="item">
												<p>Read More</p>
											</div>
										</li>
									</a>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-item-1 wow fadeIn" data-wow-duration="0.75s">
							<div class="thumb-content">

								<div class="thumb-inner">
									<a href="{{'mobile_applications'}}"><img src="resources\assets\images\services\services-homeO.gif" alt=""></a>
								</div>
							</div>
							<div class="down-content">
								<a href="{{'services'}}">
									<h4>Service Oriented Development</h4>
								</a>
								<span>Outsource Web Services Development</span>
								<p class="text-justify">
									Service Oriented Architecture (SOA) is a software design pattern supporting communication between distributed pieces of software having modularized business logic or individual functions of an application....
								</p>
								<ul class="services-info-1">
									<a href="{{'mobile_applications'}}">
										<li>
											<div class="item">
												<p>Read More</p>
											</div>
										</li>
									</a>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="services-item-1 wow fadeIn" data-wow-duration="0.75s">
							<div class="thumb-content">

								<div class="thumb-inner">
									<a href="{{'services'}}"><img src="resources\assets\images\services\tester.gif" alt=""></a>
								</div>
							</div>
							<div class="down-content">
								<a href="{{'services'}}">
									<h4>Software Testing Services</h4>
								</a>
								<span>Manual Testing Services Hire Certified Software Testers.</span>
								<p class="text-justify">
									SummationIT has built significant capabilities in the areas of testing. We know the importance of QA in a project and to achieve that we have implemented best practices....
								</p>
								<ul class="services-info-1">
									<a href="{{'mobile_applications'}}">
										<li>
											<div class="item">
												<p>Read More</p>
											</div>
										</li>
									</a>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="testimonials">
		<div class="container">
			<div id="owl-testimonials" class="owl-carousel owl-theme">
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>Antonio Andreas</h4>
						<div class="line-dec2"></div>
						<span>Car Dealer</span>
					</div>
				</div>
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>John Robertson</h4>
						<div class="line-dec2"></div>
						<span>Sale Agent</span>
					</div>
				</div>
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>Matias Lucas</h4>
						<div class="line-dec2"></div>
						<span>Mechanic</span>
					</div>
				</div>
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>Antonio Andreas</h4>
						<div class="line-dec2"></div>
						<span>Car Dealer</span>
					</div>
				</div>
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>John Robertson</h4>
						<div class="line-dec2"></div>
						<span>Sale Agent</span>
					</div>
				</div>
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>Matias Lucas</h4>
						<div class="line-dec2"></div>
						<span>Mechanic</span>
					</div>
				</div>
				<div class="item wow fadeIn" data-wow-duration="0.75s">
					<ul class="star-rating">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
					<div class="line-dec"></div>
					<p><em>"</em> Integer sed ornare lacus, at convallis velit. Curabitur hendrerit congue convallis. Nunc vel tellus ut metus tincidunt facilisis sit amet et dolor <em>"</em></p>
					<div class="author-rate">
						<img src="http://placehold.it/100x100" alt="">
						<h4>Antonio Andreas</h4>
						<div class="line-dec2"></div>
						<span>Car Dealer</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection