@extends('layouts.app')

@section('content')
  




<div id="myCarousel" class="carousel slide carousel-fade backgroundCustom" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-7 col-12 order-md-1 text-center order-2">
              <h4>Strong and Expereinced <br>ASP.NET Team.</h4>
              <p>Best ASP.NET Development Company India.
                Hire Best ASP.NET Developers.</p>
              <a href="{{'web_application'}}">Learn More</a>
            </div>
            <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\dot-net-services.png" class="mx-auto" alt="slide"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-7 col-12 order-md-1 text-center order-2">
              <h4>PHP Development<br> Company India</h4>
              <p>
                Build Custom & Dynamic web Apps with our PHP Developers.
                Hire PHP developers India. <br>
              </p>
              <a href="{{'web_application'}}">Learn More</a>
            </div>
            <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\phpServicesSl.png" class="mx-auto" alt="slide"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="mask flex-center">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-7 col-12 order-md-1 text-center order-2">
              <h4>Android App Development <br>Company India.</h4>
              <p>
                Build Android apps as per your need with our Android App Developers.
                <br>
                Hire Android app developers India.
              </p>
              <a href="{{'mobile_applications'}}">Learn More</a>
            </div>
            <div class="col-md-5 col-12 order-md-2 order-1"><img src="resources\assets\images\MobileApps.png" class="mx-auto" alt="slide"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
</div>
<!--slide end-->




<section class="bg_gray">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class=" text-center ">
          <h3>Exclusive Features</h3>
          <p>Analyse, Plan, Implement and Optimize; this is what we follow. Our workflow is simple and takes care of all your marketing needs.</p>
        </div>
        <div class="col-md-10 m-auto">
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\free_trial.png" alt="Free-Trial">
            <br>Free Trial</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\Unique_Design.png" alt="Unique Design">
            <br>Unique Design</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\Cross_Platform.png" alt="Cross-Platform">
            <br>Cross-Platform</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\User_Friendly.png" alt="User Friendly">
            <br>User Friendly</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\Mailer_Engine.png" alt="Mailer Engine">
            <br>Mailer Engine</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\Notifications.png" alt="Notifications">
            <br>Notifications</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\Free_Updates.png" alt="Free Updates">
            <br>Free Updates</div>
          <div class="box_white1 hvr-bob">
            <img src="resources\assets\images\icons\Fast_Support.png" alt="Fast Support">
            <br>Fast Support</div>
        </div>
      </div>
    </div>
  </div>
</section>




<section class="amazing-dashboard-block img_text no-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-7  col-sm-12 wow wdth_100 slideInLeft" style="visibility: visible;">
        <div class="col-md-12">
          <div class="col-md-11 pull-right">
            <br>
            <br>
            <div class=" text-left ">
              <h3>Android APP Development</h3>
            </div>
            <div class="text ">
              <p>We ensure intuitive web app design and development services. Our team builds a solution around specific requirements , budgets, and timelines of the clients. We have proven abilities in examining business challenges, understanding the client's ambitions and delivering compelling android solutions with clear value.</p>
              <div class="two_colul">
                <ul class="coloumwise">
                  <li class="hvr-bob">Multimedia API</li>
                  <li class="hvr-bob">OpenGL API</li>
                  <li class="hvr-bob">SQLite Database</li>
                  <li class="hvr-bob">Java / J2ME</li>
                  <li class="hvr-bob">Location API</li>
                  <li class="hvr-bob">Eclipse</li>
                </ul>
              </div>
              <div class="clearfix"></div>
              <br>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-12 wdth_100 no-padding wow slideInRight" style="visibility: visible;">
        <img class="img-responsive m_none" src="resources\assets\images\services\AndroidPage.png" alt="MGL Android">
      </div>
    </div>
  </div>
</section>



<section class="amazing-dashboard-block img_text no-padding ">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12 wdth_100 no-padding wow slideInLeft" style="visibility: visible;">
        <img class="img-responsive" src="resources\assets\images\services\ios-appPage.png" alt="MGL Ios">
      </div>
      <div class="col-lg-7 col-md-7  col-sm-12 wow wdth_100 slideInRight" style="visibility: visible;">
        <div class="col-md-12">
          <div class="col-md-11 pull-left">
            <br>
            <br>
            <div class=" text-left ">
              <h3>IOS APP Development</h3>
            </div>
            <div class="text ">
              <p>We deliver prominent hassle-free, innovative and cutting edge, IOS phone and IPAD app development services. Our diligent and hardworking team has developed apps that work exceptionally on all devices. We aim to deliver robust and reactive apps to our clients. We ensure that your apps utilizes the full capacity of IOS.</p>
              <div class="two_colul">
                <ul class="coloumwise">
                  <li class="hvr-bob">Mac OS X</li>
                  <li class="hvr-bob">iPhone Software Development</li>
                  <li class="hvr-bob">Interface Builder</li>
                  <li class="hvr-bob">Safari Web Kit Extensions</li>
                  <li class="hvr-bob">Objective-C</li>
                  <li class="hvr-bob">XCode-IDE</li>
                  <li class="hvr-bob">Database API</li>
                  <li class="hvr-bob">Location API</li>
                </ul>
              </div>
              <div class="clearfix"></div>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<section class="amazing-dashboard-block img_text no-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-7  col-sm-12 wow wdth_100 slideInLeft" style="visibility: visible;">
        <div class="col-md-12">
          <div class="">
            <br>
            <br>
            <div class=" text-left ">
              <h3>React Native APP Development</h3>
            </div>
            <div class="text ">
              <p>We provide customer oriented React Native customized mobile app development service. Our team comprises of professional engineers, managers and designers. We offer excellent consultancy on language, strategy and library. Our approximate time spans are specific and perfect that we strictly follow. We have our expertise in the complete React Native App Development process. We provide complete security and privacy to our clients and strong business models. We assure significant satisfaction to our customers.</p>
              <div class="two_colul">
                <ul class="coloumwise">
                  <li class="hvr-bob">AR / VR Mobile Apps</li>
                  <li class="hvr-bob">Blockchain Mobile Apps</li>
                  <li class="hvr-bob">AI / Machine Learning Apps</li>
                  <li class="hvr-bob">Location Sensing Apps</li>
                  <li class="hvr-bob">Customization Services</li>
                  <li class="hvr-bob">Biometric Sensors</li>
                </ul>
              </div>
              <div class="clearfix"></div>
              <br>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-12 wdth_100 no-padding wow slideInRight" style="visibility: visible;">
        <img class="img-responsive m_none" src="resources\assets\images\services\react-nativePage.png" alt="MGL React Native">
      </div>
    </div>
  </div>
</section>
@endsection